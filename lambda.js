const AWS = require("aws-sdk");
const lambda = new AWS.Lambda();

const awsServerlessExpress = require('aws-serverless-express');
const server = require('./dist/server');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');

const binaryMimeTypes = [
  'application/javascript',
  'application/json',
  'application/octet-stream',
  'application/xml',
  'image/jpeg',
  'image/png',
  'image/gif',
  'text/comma-separated-values',
  'text/css',
  'text/html',
  'text/javascript',
  'text/plain',
  'text/text',
  'text/xml',
  'image/x-icon',
  'image/svg+xml',
  // 'application/x-font-ttf'
];

server.app.use(awsServerlessExpressMiddleware.eventContext());

const serverProxy = awsServerlessExpress.createServer(server.app, null, binaryMimeTypes);

module.exports.universal = (event, context) => {
  if(event.httpMethod === 'POST'){
    awsServerlessExpress.proxy(serverProxy, event.body, context);
    console.log("POST REQUEST:",event);
    // console.log("POST REQUEST Body:",event.body);
    // var buf = Buffer.from(event.body, 'base64').toString('ascii');
    // console.log("REQUEST DECODE BODY:",buf);
    // var res = buf.split("=");
    // console.log(res);
    // let token = decodeURIComponent(res[1]);
    // console.log("Token:",token);
    // const params = {
    //     // not passing in a ClientContext
    //     'FunctionName': 'AWS-marketplace-registration-page',
    //     // InvocationType is RequestResponse by default
    //     // LogType is not set so we won't get the last 4K of logs from the invoked function
    //     // Qualifier is not set so we use $LATEST
    //     'InvokeArgs': JSON.stringify(event)
    // };
    // lambda.invokeAsync(params, function(err, data) {
    //     if (err) {
    //         throw (err);
    //     } else {
    //         console.log(JSON.stringify(data));
    //     }
    // });
  }
  else awsServerlessExpress.proxy(serverProxy, event, context);
};