import "zone.js/dist/zone-node";
import "reflect-metadata";
import { enableProdMode } from "@angular/core";
// Express Engine
import { ngExpressEngine } from "@nguniversal/express-engine";
// Import module map for lazy loading
import { provideModuleMap } from "@nguniversal/module-map-ngfactory-loader";

import * as express from "express";
import * as compression from "compression";
import { join } from "path";
import { readFileSync } from "fs";
require("dotenv").config();
// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
export const app = express();

app.use(compression());

const DIST_FOLDER = join(process.cwd(), "dist/browser");

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {
  AppServerModuleNgFactory,
  LAZY_MODULE_MAP,
} = require("./dist/server/main");

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine(
  "html",
  ngExpressEngine({
    bootstrap: AppServerModuleNgFactory,
    providers: [provideModuleMap(LAZY_MODULE_MAP)],
  })
);

//////////reada file
var token = "hello%20world";

let count = 0;
app.set("view engine", "html");
app.set("views", DIST_FOLDER);

// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => { });
// Server static files from /browser
app.get(
  "*.*",
  express.static(DIST_FOLDER, {
    maxAge: "1y",
  })
);

app.get("/token", (req, res) => {
  console.log("How many times do I get logged...");
  console.log("Me2:", req.headers);
  console.log("Token:", token);
  res.json(token);
});
// All regular routes use the Universal engine
app.get("*", (req, res) => {
  console.log("Count:", count);
  count++;
  if (count > 2) {
    token = "hello%20world";
  }
  let t = req["headers"]["x-apigateway-event"] as string;
  if (t.length < 400) {
    console.log("Assigning....", t);
    count = 1;
    token = t;
  }
  console.log("GET REQUEST::", req);
  console.log("TOKEN REQUEST::", token);

  res.render("index", { req: req["headers"]["x-apigateway-event"] });
});
// All regular routes use the Universal engine
app.post("*", (req, res) => {
  console.log("POST REQUEST::", req["headers"]["x-apigateway-event"]);

  res.render("index", { req: req["x-apigateway-event"] });
});
