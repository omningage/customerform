import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ConfigService } from 'src/services/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  myStr: string = 'This+is+my+string';
  arr: string[] = [];
  subscriptions:Subscription[]=[];
  constructor(private _config:ConfigService){}
  ngOnInit() {
    this.readConfigFile();
    this.arr = this.myStr.split('');
    console.log('splicedArray', this.removeMultipleItemInRange(this.arr, 5, 8));
  }

  readConfigFile() {
    this.subscriptions.push(
        this._config.onConfigRead.subscribe(() => {
        })
    );
}

  removeMultipleItemInRange(arr, prevIndex, nextIndex) {
    return [...arr].filter(
      (ele, index) => !(index > prevIndex && index < nextIndex)
    );
  }
}
