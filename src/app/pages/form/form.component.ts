import { Component, HostListener, OnInit } from '@angular/core';
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { FormService } from 'src/services/form.service';
import csc from 'country-state-city';
import {ConfigService} from 'src/services/config.service';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  currentYear = new Date().getFullYear();
  apiError: string;
  setAutoHide: boolean = true;
  autoHide: number = 20000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  TemplateLink='';

  message: number=0;

  i = 1;
  regions: any[] = [
    { Name: 'US East' },
    { Name: 'US West' },
    { Name: 'Asia Pacific (Sydney)' },
    { Name: 'Asia Pacific (Singapore)' },
    { Name: 'Asia Pacific (Tokyo)' },
    { Name: 'EU (Frankfurt)' },
    { Name: 'EU (London)' },
  ];
  Environment: any[] = [
    { environment: 'Trial' },
    { environment: 'Subscription' },
  ];
  countries: any[] = [];
  selectedCountryPhoneNumber: string;
  cities: any[] = [];
  states: any[] = [];
  enviormentDropdown: boolean = false;
  omniCustomerDetails: OmniCustomerDetail = new OmniCustomerDetail();
  errors: OmniCustomerDetail = new OmniCustomerDetail();
  formSubmitted: boolean = false;
  addExtraClass: boolean = true;

  @HostListener('document:click', ['$event'])
  onDocumentClick(event) {
    this.enviormentDropdown = false;
  }
  constructor(public config: ConfigService, public formService: FormService, public snackBar: MatSnackBar, private cookieService: CookieService) {
  }

  ngOnInit() {
    this.countries = csc.getAllCountries();
    console.log('countries', this.countries);

    this.message= parseInt(this.cookieService.get('OnFormPage'));
    if(!this.message){
      this.message=0;
    }
  }

  receiveMessage(event) {
    //;
    this.message = event;
    this.cookieService.set('OnFormPage', this.message.toString());
  }

  DeleteMessage(event){
    this.message=event.onPage;
    this.cookieService.set('OnFormPage', this.message.toString());
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    config.panelClass = this.addExtraClass ? ['test'] : undefined;
    this.snackBar.open(event.errormessage, '',config);
  }

  // next() {
  //   this.i = this.i + 1;
  //   console.log(this.omniCustomerDetails);
  // }

  // prev() {
  //   this.i = this.i - 1;
  // }

  // submitRegion() {
  //   this.errors = new OmniCustomerDetail();
  //   if (
  //     this.omniCustomerDetails.Region == null ||
  //     this.omniCustomerDetails.Region == ''
  //   ) {
  //     this.errors.Region = 'Region is required.';
  //     return;
  //   } else if (
  //     !this.regions.find(
  //       (item) => item.Name === this.omniCustomerDetails.Region
  //     )
  //   ) {
  //     this.errors.Region = 'Select a given Region.';
  //     return;
  //   }
  //   this.next();
  // }

  // submitBusinessInfo(): void {
  //   this.errors = new OmniCustomerDetail();
  //   if (
  //     this.omniCustomerDetails.CompanyName == null ||
  //     this.omniCustomerDetails.CompanyName == ''
  //   ) {
  //     this.errors.CompanyName = this.formService.requiredFieldError;
  //     return;
  //   } else if (this.omniCustomerDetails.CompanyName.length > 50) {
  //     this.errors.CompanyName =
  //       'Should be less than or equal to 50 characters.';
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.DeploymentStage ||
  //     this.omniCustomerDetails.DeploymentStage.length === 0
  //   ) {
  //     this.errors.DeploymentStage = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.AWSAccountNo ||
  //     this.omniCustomerDetails.AWSAccountNo.length === 0
  //   ) {
  //     this.errors.AWSAccountNo = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.formService.numbersRegex.test(this.omniCustomerDetails.AWSAccountNo)
  //   ) {
  //     this.errors.AWSAccountNo = 'Not a valid number.';
  //     return;
  //   } else if (this.omniCustomerDetails.AWSAccountNo.length != 12) {
  //     this.errors.AWSAccountNo = 'Not a 12 digits number.';
  //     return;
  //   }
  //   console.log(this.omniCustomerDetails);
  //   this.next();
  // }

  // submitConnectAccountDetail() {
  //   this.errors = new OmniCustomerDetail();
  //   if (
  //     !this.omniCustomerDetails.InstanceName ||
  //     this.omniCustomerDetails.InstanceName.length === 0
  //   ) {
  //     this.errors.InstanceName = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.InstanceARN ||
  //     this.omniCustomerDetails.InstanceARN.length === 0
  //   ) {
  //     this.errors.InstanceARN = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.CrossAccountRoleARN ||
  //     this.omniCustomerDetails.CrossAccountRoleARN.length === 0
  //   ) {
  //     this.errors.CrossAccountRoleARN = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.ccpURL ||
  //     this.omniCustomerDetails.ccpURL.length === 0
  //   ) {
  //     this.errors.ccpURL = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.formService.urlRegex.test(this.omniCustomerDetails.ccpURL)
  //   ) {
  //     this.errors.ccpURL = 'Enter a valid URL';
  //     return;
  //   }
  //   console.log(this.omniCustomerDetails);
  //   this.next();
  // }

  // submitDatabaseInfo() {
  //   this.errors = new OmniCustomerDetail();
  //   if (
  //     !this.omniCustomerDetails.DatabaseName ||
  //     this.omniCustomerDetails.DatabaseName.length === 0
  //   ) {
  //     this.errors.DatabaseName = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.HostName ||
  //     this.omniCustomerDetails.HostName.length === 0
  //   ) {
  //     this.errors.HostName = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.Port ||
  //     this.omniCustomerDetails.Port.length === 0
  //   ) {
  //     this.errors.Port = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.formService.numbersRegex.test(this.omniCustomerDetails.Port)
  //   ) {
  //     this.errors.Port = 'Not a valid port.';
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.UserName ||
  //     this.omniCustomerDetails.UserName.length === 0
  //   ) {
  //     this.errors.UserName = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.omniCustomerDetails.Password ||
  //     this.omniCustomerDetails.Password.length === 0
  //   ) {
  //     this.errors.Password = this.formService.requiredFieldError;
  //     return;
  //   }
  //   this.next();
  // }

  // submitForm(): void {
  //   this.errors = new OmniCustomerDetail();
  //   if (
  //     this.omniCustomerDetails.Name == null ||
  //     this.omniCustomerDetails.Name == ''
  //   ) {
  //     this.errors.Name = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.Email == null ||
  //     this.omniCustomerDetails.Email == ''
  //   ) {
  //     this.errors.Email = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.formService.emailRegex.test(this.omniCustomerDetails.Email)
  //   ) {
  //     this.errors.Email = 'Enter a valid email.';
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.Country == null ||
  //     this.omniCustomerDetails.Country == ''
  //   ) {
  //     this.errors.Country = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.countries.find(
  //       (item) => item.name === this.omniCustomerDetails.Country
  //     )
  //   ) {
  //     this.errors.Country = 'Select a given Country.';
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.State == null ||
  //     this.omniCustomerDetails.State == ''
  //   ) {
  //     this.errors.State = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.states.find((item) => item.name === this.omniCustomerDetails.State)
  //   ) {
  //     this.errors.State = 'Select a given State.';
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.City == null ||
  //     this.omniCustomerDetails.City == ''
  //   ) {
  //     this.errors.City = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.Country.toLowerCase() != 'south africa' &&
  //     !this.cities.find((item) => item.name === this.omniCustomerDetails.City)
  //   ) {
  //     this.errors.City = 'Select a given City.';
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.Contact == null ||
  //     this.omniCustomerDetails.Contact == ''
  //   ) {
  //     this.errors.Contact = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     /^\d{7,}$/.test(
  //       this.omniCustomerDetails.Contact.replace(/[\s()+\-\.]|ext/gi, '')
  //     ) == false
  //   ) {
  //     this.errors.Contact = 'Not a valid contact.';
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.ZIP_PostCode == null ||
  //     this.omniCustomerDetails.ZIP_PostCode == ''
  //   ) {
  //     this.errors.ZIP_PostCode = this.formService.requiredFieldError;
  //     return;
  //   } else if (
  //     !this.formService.numbersRegex.test(this.omniCustomerDetails.ZIP_PostCode)
  //   ) {
  //     this.errors.ZIP_PostCode = 'Not a valid zip code.';
  //     return;
  //   } else if (
  //     this.omniCustomerDetails.BusinessInfo &&
  //     this.omniCustomerDetails.BusinessInfo.length > 0 &&
  //     !this.formService.urlRegex.test(this.omniCustomerDetails.BusinessInfo)
  //   ) {
  //     this.errors.BusinessInfo = 'Enter a valid URL';
  //     return;
  //   }
  //   console.log(this.omniCustomerDetails);
  //   this.callApi();
  // }

  // callApi() {
  //   this.apiError = null;
  //   this.formService.subscribeOmniUser(this.omniCustomerDetails).subscribe(
  //     (res: any) => {
  //       console.log(res);
  //       if (res?.status === 200) {
  //         this.formSubmitted = true;
  //         this.next();
  //       } else {
  //         this.apiError = 'An error occurred while submitting data.';
  //       }
  //     },
  //     (error) => {
  //       this.apiError = 'An error occurred while submitting data.';
  //     }
  //   );
  // }

  // SelectedEnvironment(index) {
  //   this.omniCustomerDetails.DeploymentStage = this.Environment[
  //     index
  //   ].environment;
  //   this.enviormentDropdown = false;
  // }
  // SelectedCountry() {
  //   this.omniCustomerDetails.State = null;
  //   this.omniCustomerDetails.City = null;
  //   this.states = [];
  //   this.cities = [];
  //   let selectedCountry: any = this.countries.find(
  //     (item) => item.name === this.omniCustomerDetails.Country
  //   );
  //   if (selectedCountry) {
  //     this.states = csc.getStatesOfCountry(selectedCountry.id);
  //     this.selectedCountryPhoneNumber = '+' + selectedCountry.phonecode;
  //     this.omniCustomerDetails.Contact = this.selectedCountryPhoneNumber;
  //   }
  // }
  // SelectedState() {
  //   this.omniCustomerDetails.City = null;
  //   this.cities = [];
  //   let selectedState: any = this.states.find(
  //     (item) => item.name === this.omniCustomerDetails.State
  //   );
  //   if (selectedState) {
  //     this.cities = csc.getCitiesOfState(selectedState.id);
  //   }
  // }

  FinalPageShows(event){
    this.message=event.onFinalPage;
    this.cookieService.set('OnFormPage', this.message.toString());
    this.TemplateLink=event.TemplateLink;
  }
}
