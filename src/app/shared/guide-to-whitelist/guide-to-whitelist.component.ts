import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-guide-to-whitelist',
  templateUrl: './guide-to-whitelist.component.html',
  styleUrls: ['./guide-to-whitelist.component.scss']
})
export class GuideToWhitelistComponent implements OnInit {

  @Output() nextButtonEventGuideToWhitelistForm = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }


  next() {
    this.nextButtonEventGuideToWhitelistForm.emit(7)
  }

}
