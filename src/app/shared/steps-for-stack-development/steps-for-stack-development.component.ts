import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-steps-for-stack-development',
  templateUrl: './steps-for-stack-development.component.html',
  styleUrls: ['./steps-for-stack-development.component.scss']
})
export class StepsForStackDevelopmentComponent implements OnInit {

  URL_Link:string='';

  @Output() nextButtonEventStepsForStackDevelopmentForm = new EventEmitter<number>();
  @Output() prevButtonEventStepsForStackDevelopmentForm = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    if(sessionStorage.getItem('TemplateLink') != null){
      this.URL_Link=sessionStorage.getItem('TemplateLink');
    }


  }


  next() {
    this.nextButtonEventStepsForStackDevelopmentForm.emit(7)
  }

  // prev() {
  //   this.prevButtonEventStepsForStackDevelopmentForm.emit(6)
  // }

}
