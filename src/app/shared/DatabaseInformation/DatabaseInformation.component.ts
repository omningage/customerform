import { isNgTemplate } from "@angular/compiler";
import { Component, EventEmitter, HostListener, OnInit, Output } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { ConfigService } from "src/services/config.service";
import { FormService } from 'src/services/form.service';
import { SaveFormService } from "src/services/SaveForm.service";

@Component({
    selector: "app-DatabaseInformation",
    templateUrl: "./DatabaseInformation.component.html",
    styleUrls: ["./DatabaseInformation.component.scss"],
})
export class DatabaseInformationComponent implements OnInit {
    currentYear = new Date().getFullYear();
    omniCustomerDetails: OmniCustomerDetail = new OmniCustomerDetail();
    errors: OmniCustomerDetail = new OmniCustomerDetail();
    //@Output() nextButtonEventDatabaseInformationType = new EventEmitter<{onFinalPage: number, TemplateLink: string}>();
    @Output() nextButtonEventDatabaseInformationType = new EventEmitter<number>();
    @Output() prevButtonEventDatabaseInformationType = new EventEmitter<number>();
    //@Output() ErrorOccursRedirectToDatabasePage=new EventEmitter<{onPage: number, errormessage: string}>();
    VPCTypeDropdown: boolean = false;
    OMNINGAGEIP:string='';
    IPCIDRArr:any[]=[];
    ImageShown=0;
    

    nextButtonFade:boolean=false;

    VPCPeeringOBJ:any={} as any;
    TemplateCreationOBJ={} as any;
    PreFormApiCallOBJ={} as any;
    DatabaseCreationObj={} as any;

    TemplateLink='';

   
   successMessage="VPC Peering Intialized.";

    NotAvailableCIDR:any=[];

    VPCType:any[]=[
        { type: 'VPC' },
        { type: 'Public' }
    ];
    subscriptions=[] as any;

    @HostListener('document:click', ['$event'])
    onDocumentClick(event) {
    this.VPCTypeDropdown = false;
  }
  
   constructor(private saveForm: SaveFormService,public formService: FormService, private cookieService: CookieService, public config:ConfigService){}
    ngOnInit() {
      if(this.cookieService.get('HostName')){
        this.omniCustomerDetails.ISVPCOrPublic=this.cookieService.get('ISVPCOrPublic');
        this.omniCustomerDetails.HostName=this.cookieService.get('HostName');
        this.omniCustomerDetails.Port=this.cookieService.get('Port');
        this.omniCustomerDetails.UserName=this.cookieService.get('UserName');
        this.omniCustomerDetails.Password=this.cookieService.get('Password');
        if(this.omniCustomerDetails.ISVPCOrPublic == 'VPC'){
            this.omniCustomerDetails.VPCId=this.cookieService.get('VPCId');
            this.omniCustomerDetails.CIDRIP=this.cookieService.get('CIDRIP');
            this.omniCustomerDetails.SecurityGroupId=this.cookieService.get('SecurityGroupId');
            this.omniCustomerDetails.RouteTableID=this.cookieService.get('RouteTableID');
            this.omniCustomerDetails.SecurityGroupIdForLambda=this.cookieService.get('SecurityGroupIdForLambda');
            this.omniCustomerDetails.SubnetID=this.cookieService.get('Subnet');
        }

        this.nextButtonFade=true;
    }
    this.subscriptions.push(
      this.config.onConfigRead.subscribe(() => {
        this.CallApiCIDR();
      })
    );

    }

    CallApiCIDR(){
        this.saveForm.getOccupiedCIDR().subscribe((result:any)=>{
            if(result){
                this.OMNINGAGEIP=result.OmningageIP;
                this.IPCIDRArr= result.IP;

                this.NotAvailableCIDR.push(this.OMNINGAGEIP);
                for(var i=0;i<this.IPCIDRArr.length;i++){
                  this.NotAvailableCIDR.push(this.IPCIDRArr[i].cidrIP);
                }

                sessionStorage.setItem("CIDRIPArray", this.NotAvailableCIDR);

                console.log('NotAvaialbleCIDR',this.NotAvailableCIDR);
            }

        });
    }



      SelectedVpcType(index) {
        this.omniCustomerDetails.ISVPCOrPublic = this.VPCType[index].type;
        this.VPCTypeDropdown = false;
        this.nextButtonFade=false;
      }

    submitDatabaseInfo() {
      var RegularExpression=/^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/;
        this.errors = new OmniCustomerDetail();
        if(this.omniCustomerDetails.HostName){
            var result = this.omniCustomerDetails.HostName.match(".rds.amazonaws.com");
        }
        if (
          !this.omniCustomerDetails.HostName ||
          this.omniCustomerDetails.HostName.length === 0
        ) {
          this.errors.HostName = this.formService.requiredFieldError;
          return;
        }
        else if(!result){
                this.errors.HostName="Please enter a valid host name.";
                return;
        }
         else if (
          !this.omniCustomerDetails.Port ||
          this.omniCustomerDetails.Port.length === 0
        ) {
          this.errors.Port = this.formService.requiredFieldError;
          return;
        } else if (
          !this.formService.numbersRegex.test(this.omniCustomerDetails.Port)
        ) {
          this.errors.Port = 'Not a valid port.';
          return;
        } else if (
          !this.omniCustomerDetails.UserName ||
          this.omniCustomerDetails.UserName.length === 0
        ) {
          this.errors.UserName = this.formService.requiredFieldError;
          return;
        } else if (
          !this.omniCustomerDetails.Password ||
          this.omniCustomerDetails.Password.length === 0
        ) {
          this.errors.Password = this.formService.requiredFieldError;
          return;
        }
        else if (
            !this.omniCustomerDetails.ISVPCOrPublic ||
            this.omniCustomerDetails.ISVPCOrPublic.length === 0
          ) {
            this.errors.ISVPCOrPublic = this.formService.requiredFieldError;
            return;
          }
        if(this.omniCustomerDetails.ISVPCOrPublic=='VPC'){
          if(this.omniCustomerDetails.VPCId){
            var resultVPC = this.omniCustomerDetails.VPCId.match("vpc-");
          }
          if(this.omniCustomerDetails.SecurityGroupId){
            var resultSecurityGroupId=this.omniCustomerDetails.SecurityGroupId.match('sg-');
          }
          if(this.omniCustomerDetails.RouteTableID){
            var resultRouteTableID=this.omniCustomerDetails.RouteTableID.match('rtb-');
          }
          if(this.omniCustomerDetails.SecurityGroupIdForLambda){
            var resultSecurityGroupIdForLambda=this.omniCustomerDetails.SecurityGroupId.match('sg-');
          }
          if(this.omniCustomerDetails.SubnetID){
            var resultSubnetID=this.omniCustomerDetails.SubnetID.match('subnet-');
          }
            if (
                !this.omniCustomerDetails.VPCId ||
                this.omniCustomerDetails.VPCId.length === 0
              ) {
                this.errors.VPCId = this.formService.requiredFieldError;
                return;
              }else if(this.omniCustomerDetails.VPCId.length != 21){
                this.errors.VPCId='Please enter a valid VPC id.';
                return;
              }else if(!resultVPC){
                this.errors.VPCId='Please enter a valid VPC id';
                return;
              } else if (
                !this.omniCustomerDetails.CIDRIP ||
                this.omniCustomerDetails.CIDRIP.length === 0
              ) {
                this.errors.CIDRIP = this.formService.requiredFieldError;
                return;
              }else if(this.NotAvailableCIDR.find((item:any)=> item ==this.omniCustomerDetails.CIDRIP)){
                this.errors.CIDRIP='this IP is already occupied. Pleae try with another one.';
                return;
              } else if(!RegularExpression.test(this.omniCustomerDetails.CIDRIP)){
                this.errors.CIDRIP='Please enter a valid CIDRIP';
                return;
              }
              else if (
                !this.omniCustomerDetails.SecurityGroupId ||
                this.omniCustomerDetails.SecurityGroupId.length === 0
              ) {
                this.errors.SecurityGroupId = this.formService.requiredFieldError;
                return;
              }else if(this.omniCustomerDetails.SecurityGroupId.length != 20){
                this.errors.SecurityGroupId='Please enter a valid Security Group id.';
                return;
              }else if(!resultSecurityGroupId){
                this.errors.SecurityGroupId='Please enter a valid Security Group id.';
                return;
              }
              else if (
                !this.omniCustomerDetails.RouteTableID ||
                this.omniCustomerDetails.RouteTableID.length === 0
              ) {
                this.errors.RouteTableID = this.formService.requiredFieldError;
                return;
              }
              else if(this.omniCustomerDetails.RouteTableID.length!=21){
                this.errors.RouteTableID = 'Please enter a valid RouteTable id.';
                return;
              }else if(!resultRouteTableID){
                this.errors.RouteTableID='Please enter a valid RouteTable id.';
                return;
              }
              else if(!this.omniCustomerDetails.SecurityGroupIdForLambda && this.omniCustomerDetails.SecurityGroupIdForLambda.length==0){
                this.errors.SecurityGroupIdForLambda=this.formService.requiredFieldError;
              }
              else if(this.omniCustomerDetails.SecurityGroupIdForLambda.length != 20){
                this.errors.RouteTableID = 'Please enter a valid security group id for lambda.';
                return;
              }else if(!resultSecurityGroupIdForLambda){
                this.errors.SecurityGroupIdForLambda='Please enter a valid security group id for lambda.';
                return;
              }

              else if(!this.omniCustomerDetails.SubnetID && this.omniCustomerDetails.SubnetID.length==0){
                this.errors.SubnetID=this.formService.requiredFieldError;
              }
              else if(this.omniCustomerDetails.SubnetID.length != 24){
                this.errors.SubnetID = 'Please enter a valid subnet id.';
                return;
              }else if(!resultSubnetID){
                this.errors.SubnetID='Please enter a valid subnet id.';
                return;
              }
              
        }
        this.SetDatabaseInformationForm();
      }


      next() {
        // let obj={
        //   onFinalPage: 5,
        //   TemplateLink: this.TemplateLink
        // }
          this.nextButtonEventDatabaseInformationType.emit(5)
        }
      
        prev() {
          this.prevButtonEventDatabaseInformationType.emit(3)
        }

      SetDatabaseInformationForm(){
        this.cookieService.set('HostName', this.omniCustomerDetails.HostName);
        this.cookieService.set('Port', this.omniCustomerDetails.Port);
        this.cookieService.set('UserName', this.omniCustomerDetails.UserName);
        this.cookieService.set('Password', this.omniCustomerDetails.Password);
        this.cookieService.set('ISVPCOrPublic', this.omniCustomerDetails.ISVPCOrPublic);
        if(this.omniCustomerDetails.ISVPCOrPublic=='VPC'){
            this.cookieService.set('VPCId', this.omniCustomerDetails.VPCId);
            this.cookieService.set('CIDRIP', this.omniCustomerDetails.CIDRIP);
            this.cookieService.set('SecurityGroupId', this.omniCustomerDetails.SecurityGroupId);
            this.cookieService.set('RouteTableID', this.omniCustomerDetails.RouteTableID);
            this.cookieService.set('SecurityGroupIdForLambda', this.omniCustomerDetails.SecurityGroupIdForLambda);
            this.cookieService.set('Subnet', this.omniCustomerDetails.SubnetID);
          }

          this.next();
      }



    CheckContinueButton(){
      this.nextButtonFade=false;
      if(this.omniCustomerDetails.ISVPCOrPublic){
        if(this.omniCustomerDetails.ISVPCOrPublic=='VPC'){
          if(this.omniCustomerDetails.HostName && this.omniCustomerDetails.Port && this.omniCustomerDetails.UserName && this.omniCustomerDetails.Password && this.omniCustomerDetails.VPCId && this.omniCustomerDetails.CIDRIP && this.omniCustomerDetails.UserName && this.omniCustomerDetails.SecurityGroupId && this.omniCustomerDetails.RouteTableID && this.omniCustomerDetails.SubnetID && this.omniCustomerDetails.SecurityGroupIdForLambda){
            this.nextButtonFade=true;
          }
        }else if(this.omniCustomerDetails.ISVPCOrPublic=='Public'){
          if(this.omniCustomerDetails.HostName && this.omniCustomerDetails.Port && this.omniCustomerDetails.UserName && this.omniCustomerDetails.Password){
            this.nextButtonFade=true;
          }
        }
      }

    }
}
