import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
// import { MaterialModule } from "../material-module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StringFilterPipe } from "./pipes/string-filter.pipe";
import { NumbersOnlyDirective } from "./directives/numbers-only.directive";
import { BussinessInformationComponent } from "./BussinessInformation/BussinessInformation.component";
import { BussinessInformationDetailsComponent } from "./BussinessInformationDetails/BussinessInformationDetails.component";
import {AWSConnectFormComponent} from "./AWSConnectForm/AWSConnectForm.component";
import {DatabaseInformationComponent} from "./DatabaseInformation/DatabaseInformation.component";
import {SubmissionFormComponent} from "./SubmissionForm/SubmissionForm.component";
import { TextHighlighterPipe } from "./pipes/text-highlighter.pipe";
//import { CookieService } from "ngx-cookie-service";
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { StepperWizardComponent } from './stepper-wizard/stepper-wizard.component';
import {ProcessingFormComponent} from './ProcessingForm/ProcessingForm.component';
import {S3BucketInformationFormComponent} from './S3BucketInformationForm/S3BucketInformationForm.component';
import {GuideToWhitelistComponent} from "./guide-to-whitelist/guide-to-whitelist.component";
import {StepsForStackDevelopmentComponent} from "./steps-for-stack-development/steps-for-stack-development.component";
import {SuccessComponent} from "./success/success.component";
import {VerificationComponent} from "./verification/verification.component";
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
    declarations: [
        StringFilterPipe,
        NumbersOnlyDirective,
        BussinessInformationComponent,
        BussinessInformationDetailsComponent,
        AWSConnectFormComponent,
        DatabaseInformationComponent,
        S3BucketInformationFormComponent,
        SubmissionFormComponent,
        TextHighlighterPipe,
        StepperWizardComponent,
        ProcessingFormComponent,
        GuideToWhitelistComponent,
        StepsForStackDevelopmentComponent,
        SuccessComponent,
        VerificationComponent,
    ],
    imports: [
        CommonModule,
        //MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTooltipModule,
        MatMenuModule,
        MatRadioModule,

    ],
    exports: [
        CommonModule,
        //MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        StringFilterPipe,
        BussinessInformationComponent,
        BussinessInformationDetailsComponent,
        AWSConnectFormComponent,
        S3BucketInformationFormComponent,
        DatabaseInformationComponent,
        SubmissionFormComponent,
        ProcessingFormComponent,
        GuideToWhitelistComponent,
        StepsForStackDevelopmentComponent,
        NumbersOnlyDirective,
        SuccessComponent,
        VerificationComponent,
        TextHighlighterPipe
    ],
    providers: [],
    entryComponents: [],
})
export class SharedModule { }
