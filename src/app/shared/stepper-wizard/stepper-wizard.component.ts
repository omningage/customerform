import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-stepper-wizard',
  templateUrl: './stepper-wizard.component.html',
  styleUrls: ['./stepper-wizard.component.scss']
})
export class StepperWizardComponent implements OnInit {
  @Input() public inputwizard=0;
  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(){
    this.inputwizard=this.inputwizard;
  }

}
