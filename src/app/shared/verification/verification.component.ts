import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { FormService } from 'src/services/form.service';
import { SaveFormService } from 'src/services/SaveForm.service';
import csc from 'country-state-city';
import { ConfigService } from 'src/services/config.service';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {
  currentYear = new Date().getFullYear();
  omniCustomerDetails: OmniCustomerDetail = new OmniCustomerDetail();
  errors: OmniCustomerDetail = new OmniCustomerDetail();

  buttonDisabled:boolean=false;
  @Output() nextButtonEventVerificationForm = new EventEmitter<{onFinalPage: number, TemplateLink: string}>();
  @Output() prevButtonEventVerificationForm = new EventEmitter<number>();
  @Output() ErrorOccursRedirectToDatabasePage=new EventEmitter<{onPage: number, errormessage: string}>();


  VPCPeeringOBJ:any={} as any;
  TemplateCreationOBJ={} as any;
  PreFormApiCallOBJ={} as any;
  DatabaseCreationObj={} as any;

  loading=false;

  ImageShown=0;
  TemplateLink='';

  enviormentDropdown: boolean = false;
  RegionDropdown: boolean = false;
  CountryPhoneCode:string='+1';

  NotAvailableCIDR:any=[];

  successMessage="VPC Peering Intialized.";

  countries: any[] = [];

  Environment: any[] = [
    { environment: 'Trial (14 days free trial)' },
    { environment: 'Business (This is a paid premium account)' },
  ];

  CompanyObject={} as any;

  PersonalEmailList=['gmail.com', 'outlook.com','hotmail.com', 'yahoo.com', 'proton.com'];
  regions: any[] = [];
  regionCode:string;

  OMNINGAGEIP:string='';
  IPCIDRArr:any[]=[];
  SubDomainList=[] as any;

  subscriptions = [];
  @HostListener('document:click', ['$event'])
  onDocumentClick(event) {
  this.enviormentDropdown = false;
  this.RegionDropdown = false;
}

  constructor(private saveForm: SaveFormService,public formService: FormService,private cookieService: CookieService, public config:ConfigService) { }

  ngOnInit(): void {
    this.countries = csc.getAllCountries();
    if(this.cookieService.get('DeploymentStage') && this.cookieService.get('CompanyName') && this.cookieService.get('AWSAccountNumber') && this.cookieService.get('S3Type') && this.cookieService.get('HostName')){
      this.omniCustomerDetails.DeploymentStage=this.cookieService.get('DeploymentStage');
      this.omniCustomerDetails.SubDomain=this.cookieService.get('SubDomain');
      this.omniCustomerDetails.CompanyName=this.cookieService.get('CompanyName');
      this.omniCustomerDetails.Email=this.cookieService.get('CompanyEmail');
      this.omniCustomerDetails.Country=this.cookieService.get('Country');
      this.omniCustomerDetails.State=this.cookieService.get('State');
      this.omniCustomerDetails.City=this.cookieService.get('City');
      this.omniCustomerDetails.Contact=this.cookieService.get('Contact');
      this.CountryPhoneCode=this.cookieService.get('CountryCode');
      this.omniCustomerDetails.ZIP_PostCode=this.cookieService.get('ZIPCode');
      this.omniCustomerDetails.BusinessAddress=this.cookieService.get('BusinessAddress');


      this.omniCustomerDetails.AWSAccountNumber=this.cookieService.get('AWSAccountNumber');
      this.omniCustomerDetails.ccpURL=this.cookieService.get('ccpURL');
      this.omniCustomerDetails.InstanceName=this.cookieService.get('InstanceName');
      this.omniCustomerDetails.CrossAccountRoleARN=this.cookieService.get('CrossAccountRoleARN');
      this.omniCustomerDetails.Region=this.cookieService.get('RegionName');
      this.omniCustomerDetails.InstanceARN=this.cookieService.get('InstanceARN');
      this.omniCustomerDetails.CtrKinesisARN=this.cookieService.get('CtrKinesisARN');
      this.omniCustomerDetails.AgentKinesisARN=this.cookieService.get('AgentKinesisARN');
      this.regionCode=this.cookieService.get('Region');


      this.omniCustomerDetails.S3Type=this.cookieService.get('S3Type');
      this.omniCustomerDetails.BucketNameForCallRecording=this.cookieService.get('S3BucketCallRecording');
      if(this.omniCustomerDetails.S3Type=='No'){
        this.omniCustomerDetails.BucketNameForChatAttachment=this.cookieService.get('S3BucketChatAttachment');
        this.omniCustomerDetails.BucketNameForChatTranscript=this.cookieService.get('S3BucketChatTranscript');
      }

      this.omniCustomerDetails.ISVPCOrPublic=this.cookieService.get('ISVPCOrPublic');
      this.omniCustomerDetails.HostName=this.cookieService.get('HostName');
      this.omniCustomerDetails.Port=this.cookieService.get('Port');
      this.omniCustomerDetails.UserName=this.cookieService.get('UserName');
      this.omniCustomerDetails.Password=this.cookieService.get('Password');
      if(this.omniCustomerDetails.ISVPCOrPublic == 'VPC'){
          this.omniCustomerDetails.VPCId=this.cookieService.get('VPCId');
          this.omniCustomerDetails.CIDRIP=this.cookieService.get('CIDRIP');
          this.omniCustomerDetails.SecurityGroupId=this.cookieService.get('SecurityGroupId');
          this.omniCustomerDetails.RouteTableID=this.cookieService.get('RouteTableID');
          this.omniCustomerDetails.SecurityGroupIdForLambda=this.cookieService.get('SecurityGroupIdForLambda');
          this.omniCustomerDetails.SubnetID=this.cookieService.get('Subnet');
      }

      this.subscriptions.push(
        this.config.onConfigRead.subscribe(() => {
          this.CallApiCIDR();
          this.gettingRegions();
          this.CallCompanyName();
          this.CallSubDomain();
        })
      );
    }
  }
  
  gettingRegions(){
      this.regions=JSON.parse(sessionStorage.getItem("RegionsArray"));
      this.saveForm.getRegions().subscribe( (result:any) =>{
        if(result){
          this.regions=result;
          sessionStorage.setItem("RegionsArray", JSON.stringify(this.regions));
      }
      console.log(result);
    });
  }

  CallApiCIDR(){
    this.saveForm.getOccupiedCIDR().subscribe((result:any)=>{
        if(result){
            this.OMNINGAGEIP=result.OmningageIP;
            this.IPCIDRArr= result.IP;

            this.NotAvailableCIDR.push(this.OMNINGAGEIP);
            for(var i=0;i<this.IPCIDRArr.length;i++){
              this.NotAvailableCIDR.push(this.IPCIDRArr[i].cidrIP);
            }

            sessionStorage.setItem("CIDRIPArray", this.NotAvailableCIDR);

            console.log('NotAvaialbleCIDR',this.NotAvailableCIDR);
        }

    });
}

CallCompanyName(){
  ;
  this.saveForm.getCompanyName().subscribe((result:any)=>{
      if(result){
        this.CompanyObject = result;
      }

  });
}

CallSubDomain(){
  ;
  this.saveForm.getSubDomain().subscribe((result:any)=>{
      if(result){
          console.log('SUBDOAMIN--------------',result);
          this.SubDomainList=result;
      }

  });
}


  SelectedEnvironment(index) {
    this.omniCustomerDetails.DeploymentStage = this.Environment[
      index
    ].environment;
    this.enviormentDropdown = false;
  }


  SelectedRegionType(index) {
    this.omniCustomerDetails.Region = this.regions[index].region;
    this.regionCode=this.regions[index].code;
    this.RegionDropdown = false;
  }


  next() {
    let obj={
          onFinalPage: 6,
          TemplateLink: this.TemplateLink
        }
      this.nextButtonEventVerificationForm.emit(obj)
    }
  
    prev() {
      this.prevButtonEventVerificationForm.emit(4)
    }


    submitBusinessInfo(onFocusOut:boolean) {
      var RegularExpression=/^[1-9][0-9]{0,9}$/;
      if(onFocusOut){
        this.errors= new OmniCustomerDetail();
      }
      if (
        this.omniCustomerDetails.CompanyName == null ||
        this.omniCustomerDetails.CompanyName == ''
      ) {
        this.errors.CompanyName = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      } else if (this.omniCustomerDetails.CompanyName.length > 100) {
        this.errors.CompanyName =
          'Should be less than or equal to 100 characters.';
          this.buttonDisabled=true;
        return;
      }else if(this.CompanyObject.find((item:any)=> item.CompanyName == this.omniCustomerDetails.CompanyName)){
        this.errors.CompanyName =
        'Already exists. Please try with another name.';
          this.buttonDisabled=true;
          return false;
      } 
      else if (
        this.omniCustomerDetails.Email == null ||
        this.omniCustomerDetails.Email == ''
      ) {
        this.errors.Email = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      } else if (
        !this.formService.emailRegex.test(this.omniCustomerDetails.Email)
      ) {
        this.errors.Email = 'Enter a valid email.';
        this.buttonDisabled=true;
        return;
      }else if(!this.CheckIsPrivateEmail(this.omniCustomerDetails.Email)){
        this.errors.Email="Please enter company email."
        this.buttonDisabled=true;
        return;
      }
       else if (
        this.omniCustomerDetails.Country == null ||
        this.omniCustomerDetails.Country == ''
      ) {
        this.errors.Country = this.formService.requiredFieldError;
        return;
      } else if (
        !this.countries.find(
          (item) => item.name === this.omniCustomerDetails.Country
        )
      ) {
        this.errors.Country = 'Select a given Country.';
        this.buttonDisabled=true;
        return;
      } else if (
        this.omniCustomerDetails.State == null ||
        this.omniCustomerDetails.State == ''
      ) {
        this.errors.State = this.formService.requiredFieldError;
        return;
      }else if (
        this.omniCustomerDetails.City == null ||
        this.omniCustomerDetails.City == ''
      ) {
        this.errors.City = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      } else if (
        this.omniCustomerDetails.Contact == null ||
        this.omniCustomerDetails.Contact == ''
      ) {
        this.errors.Contact = this.formService.requiredFieldError;
        return;
      } else if (
        /^\d{7,}$/.test(
          this.omniCustomerDetails.Contact.replace(/[\s()+\-\.]|ext/gi, '')
        ) == false
      ) {
        this.errors.Contact = 'Not a valid contact.';
        this.buttonDisabled=true;
        return;
      } else if(!RegularExpression.test(this.omniCustomerDetails.Contact)){
        this.errors.Contact = 'Not a valid contact.';
        return;
      }
      else if (
        this.omniCustomerDetails.ZIP_PostCode == null ||
        this.omniCustomerDetails.ZIP_PostCode == ''
      ) {
        this.errors.ZIP_PostCode = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      } else if (
        !this.formService.numbersRegex.test(this.omniCustomerDetails.ZIP_PostCode)
      ) {
        this.errors.ZIP_PostCode = 'Not a valid zip code.';
        this.buttonDisabled=true;
        return;
      } else  if (
        this.omniCustomerDetails.BusinessAddress == null ||
        this.omniCustomerDetails.BusinessAddress == ''
      ) {
        this.errors.BusinessAddress = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      } else if (this.omniCustomerDetails.BusinessAddress.length > 250) {
        this.errors.CompanyName =
          'Should be less than or equal to 250 characters.';
          this.buttonDisabled=true;
        return;
      }
      return true;
    }


    submitDeploymentType() {
      ;
      this.errors= new OmniCustomerDetail();
      if (
        !this.omniCustomerDetails.DeploymentStage ||
        this.omniCustomerDetails.DeploymentStage.length === 0
      ) {
        this.errors.DeploymentStage = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      }
      else if (
        !this.omniCustomerDetails.SubDomain ||
        this.omniCustomerDetails.SubDomain.length === 0
      ) {
        this.errors.SubDomain = this.formService.requiredFieldError;
        return;
      }
      else if (
        this.omniCustomerDetails.SubDomain.length > 20
      ) {
        this.errors.SubDomain = "Sub domain should be less than 20 characters.";
        return;
      }
      else if(/[^a-zA-Z0-9]/.test(this.omniCustomerDetails.SubDomain)){
        this.errors.SubDomain = "Sub domain is not valid";
        return;
      }
      else if(this.SubDomainList.find((item:any)=> item.subDomain == this.omniCustomerDetails.SubDomain)){
        this.errors.SubDomain="This Domain already exists. Please try another sub domain"
        return;
      }
      return true;
    }

    submitConnectAccountDetail(onFocusOut:boolean) {
      if(onFocusOut){
        this.errors= new OmniCustomerDetail();
      }
      if(this.omniCustomerDetails.CrossAccountRoleARN){
        var resultCrossAccountValidation=this.omniCustomerDetails.CrossAccountRoleARN.match('arn:aws:iam::' + this.omniCustomerDetails.AWSAccountNumber + ':role/');
      }
      if(this.omniCustomerDetails.ccpURL){
        var resultCCPURLV1=this.omniCustomerDetails.ccpURL.match('.awsapps.com');
        if(!resultCCPURLV1){
          resultCCPURLV1=this.omniCustomerDetails.ccpURL.match('.my.connect.aws/');
        }
      }
      if(this.omniCustomerDetails.InstanceARN){
        var resultInstanceARN=this.omniCustomerDetails.InstanceARN.match('arn:aws:connect:'+ this.regionCode + ':' + this.omniCustomerDetails.AWSAccountNumber + ':instance/');
      }
      if (
          !this.omniCustomerDetails.AWSAccountNumber ||
          this.omniCustomerDetails.AWSAccountNumber.length === 0
        ) {
          this.errors.AWSAccountNumber = this.formService.requiredFieldError;
          this.buttonDisabled=true;
          return;
        } else if (!this.formService.numbersRegex.test(this.omniCustomerDetails.AWSAccountNumber)) {
          this.errors.AWSAccountNumber = this.formService.requiredFieldError;
          return;
        }else if (this.omniCustomerDetails.AWSAccountNumber.length!=12){
            this.errors.AWSAccountNumber="Please add 12 digits account number.";
            this.buttonDisabled=true;
            return;
        }else if (
          !this.omniCustomerDetails.ccpURL ||
          this.omniCustomerDetails.ccpURL.length === 0
        ) {
          this.errors.ccpURL = this.formService.requiredFieldError;
          this.buttonDisabled=true;
          return;
        } else if (
          !this.formService.urlRegex.test(this.omniCustomerDetails.ccpURL)
        ) {
          this.errors.ccpURL = 'Enter a valid URL';
          return;
        }else if(!resultCCPURLV1){
          this.errors.ccpURL='Please enter a valid URL.';
          this.buttonDisabled=true;
          return;
        }
        else if (
        !this.omniCustomerDetails.InstanceName ||
        this.omniCustomerDetails.InstanceName.length === 0
      ) {
        this.errors.InstanceName = this.formService.requiredFieldError;
        return;
      } else if(!this.omniCustomerDetails.CrossAccountRoleARN || this.omniCustomerDetails.CrossAccountRoleARN.length==0){
          this.errors.CrossAccountRoleARN=this.formService.requiredFieldError;
          this.buttonDisabled=true;
          return;
      } else if(!resultCrossAccountValidation){
        this.errors.CrossAccountRoleARN='Please enter a valid Cross Account Role ARN.';
        this.buttonDisabled=true;
        return;
      }
      else if (
          this.omniCustomerDetails.Region == null ||
          this.omniCustomerDetails.Region == ''
        ) {
          this.errors.Region = 'Region is required.';
          this.buttonDisabled=true;
          return;
        } else if (
        !this.omniCustomerDetails.InstanceARN ||
        this.omniCustomerDetails.InstanceARN.length === 0
      ) {
        this.errors.InstanceARN = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      }else if(!resultInstanceARN){
        this.errors.InstanceARN='Please enter a valid Instance ARN.';
        this.buttonDisabled=true;
        return;
      } 
      else if (
        !this.omniCustomerDetails.CtrKinesisARN ||
        this.omniCustomerDetails.CtrKinesisARN.length === 0
      ) {
        this.errors.CtrKinesisARN = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      }
      else if (
          !this.omniCustomerDetails.AgentKinesisARN ||
          this.omniCustomerDetails.AgentKinesisARN.length === 0
        ) {
          this.errors.AgentKinesisARN = this.formService.requiredFieldError;
          this.buttonDisabled=true;
          return;
        }

        return true;
    }

    submitS3BucketDetails(onFocusOut:boolean) {
      if(this.omniCustomerDetails.S3Type== 'Yes'){
        if (
          !this.omniCustomerDetails.BucketNameForCallRecording ||
          this.omniCustomerDetails.BucketNameForCallRecording.length === 0
          ) {
            this.errors.BucketNameForCallRecording = this.formService.requiredFieldError;
            this.buttonDisabled=true;
            return;
          }
        }
        else if(this.omniCustomerDetails.S3Type== 'No'){
          if (
            !this.omniCustomerDetails.BucketNameForCallRecording ||
            this.omniCustomerDetails.BucketNameForCallRecording.length === 0
            ) {
              this.errors.BucketNameForCallRecording = this.formService.requiredFieldError;
              this.buttonDisabled=true;
              return;
            }
            else  if (
              !this.omniCustomerDetails.BucketNameForChatTranscript ||
              this.omniCustomerDetails.BucketNameForChatTranscript.length === 0
            ) {
              this.errors.BucketNameForChatTranscript = this.formService.requiredFieldError;
              this.buttonDisabled=true;
              return;
            }
            else  if (
              !this.omniCustomerDetails.BucketNameForChatAttachment ||
              this.omniCustomerDetails.BucketNameForChatAttachment.length === 0
            ) {
              this.errors.BucketNameForChatAttachment = this.formService.requiredFieldError;
              this.buttonDisabled=true;
              return;
            }

        }
     return true;
    }


    submitDatabaseInfo(onFocusOut:boolean) {
      if(onFocusOut){
        this.errors= new OmniCustomerDetail();
      }
      if(this.omniCustomerDetails.HostName){
          var result = this.omniCustomerDetails.HostName.match(".rds.amazonaws.com");
      }
      if (
        !this.omniCustomerDetails.HostName ||
        this.omniCustomerDetails.HostName.length === 0
      ) {
        this.errors.HostName = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      }
      else if(!result){
              this.errors.HostName="Please enter a valid host name.";
              this.buttonDisabled=true;
              return;
      }
       else if (
        !this.omniCustomerDetails.Port ||
        this.omniCustomerDetails.Port.length === 0
      ) {
        this.errors.Port = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      } else if (
        !this.formService.numbersRegex.test(this.omniCustomerDetails.Port)
      ) {
        this.errors.Port = 'Not a valid port.';
        this.buttonDisabled=true;
        return;
      } else if (
        !this.omniCustomerDetails.UserName ||
        this.omniCustomerDetails.UserName.length === 0
      ) {
        this.errors.UserName = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      } else if (
        !this.omniCustomerDetails.Password ||
        this.omniCustomerDetails.Password.length === 0
      ) {
        this.errors.Password = this.formService.requiredFieldError;
        this.buttonDisabled=true;
        return;
      }

      return true;
    }



    functionForErrorVerification(){
      ;
      let error1=this.submitDeploymentType();
      let error2=this.submitBusinessInfo(false);
      let error3=this.submitConnectAccountDetail(false);
      let error4=this.submitS3BucketDetails(false);
      let error5=this.submitDatabaseInfo(false);

      if(error1 && error2 && error3 && error4 && error5){
        this.cookieService.set('DeploymentStage', this.omniCustomerDetails.DeploymentStage);
        this.cookieService.set("SubDomain", this.omniCustomerDetails.SubDomain);

        this.cookieService.set('CompanyName', this.omniCustomerDetails.CompanyName);
        this.cookieService.set('CompanyEmail', this.omniCustomerDetails.Email);
        this.cookieService.set('Country', this.omniCustomerDetails.Country);
        this.cookieService.set('State', this.omniCustomerDetails.State);
        this.cookieService.set('City', this.omniCustomerDetails.City);
        this.cookieService.set('ZIPCode', this.omniCustomerDetails.ZIP_PostCode);
        this.cookieService.set('Contact', this.omniCustomerDetails.Contact);
        this.cookieService.set('CountryCode', this.CountryPhoneCode);
        this.cookieService.set('BusinessAddress', this.omniCustomerDetails.BusinessAddress);

        this.cookieService.set('AWSAccountNumber', this.omniCustomerDetails.AWSAccountNumber);
        this.cookieService.set('ccpURL', this.omniCustomerDetails.ccpURL);
        this.cookieService.set('InstanceName', this.omniCustomerDetails.InstanceName);
        this.cookieService.set('CrossAccountRoleARN', this.omniCustomerDetails.CrossAccountRoleARN);
        this.cookieService.set('RegionName', this.omniCustomerDetails.Region);
        this.cookieService.set('Region', this.regionCode);
        this.cookieService.set('InstanceARN', this.omniCustomerDetails.InstanceARN);
        this.cookieService.set('CtrKinesisARN', this.omniCustomerDetails.CtrKinesisARN);
        this.cookieService.set('AgentKinesisARN', this.omniCustomerDetails.AgentKinesisARN);


        this.cookieService.set('S3Type',this.omniCustomerDetails.S3Type);
        this.cookieService.set('S3BucketCallRecording', this.omniCustomerDetails.BucketNameForCallRecording);
        if(this.omniCustomerDetails.S3Type=='No'){
          this.cookieService.set('S3BucketChatAttachment', this.omniCustomerDetails.BucketNameForChatAttachment);
          this.cookieService.set('S3BucketChatTranscript', this.omniCustomerDetails.BucketNameForChatTranscript);
        }

        this.cookieService.set('HostName', this.omniCustomerDetails.HostName);
        this.cookieService.set('Port', this.omniCustomerDetails.Port);
        this.cookieService.set('UserName', this.omniCustomerDetails.UserName);
        this.cookieService.set('Password', this.omniCustomerDetails.Password);
        this.cookieService.set('ISVPCOrPublic', this.omniCustomerDetails.ISVPCOrPublic);
        if(this.omniCustomerDetails.ISVPCOrPublic=='VPC'){
            this.cookieService.set('VPCId', this.omniCustomerDetails.VPCId);
            this.cookieService.set('CIDRIP', this.omniCustomerDetails.CIDRIP);
            this.cookieService.set('SecurityGroupId', this.omniCustomerDetails.SecurityGroupId);
            this.cookieService.set('RouteTableID', this.omniCustomerDetails.RouteTableID);
            this.cookieService.set('SecurityGroupIdForLambda', this.omniCustomerDetails.SecurityGroupIdForLambda);
            this.cookieService.set('Subnet', this.omniCustomerDetails.SubnetID);
          }

          this.func();
      }
    }

    func(){
      ;
      this.loading=true;
      if(this.omniCustomerDetails.ISVPCOrPublic=='VPC'){
            this.VPCPeeringOBJ=this.ObjectCreationForVPCPeering();
            this.saveForm.VPCPeering(this.VPCPeeringOBJ).subscribe((res: any) => {
                ;
                console.log("Responds2", res);
                if(res && res.status==200){
                  this.ImageShown=1;
                  this.successMessage=res.body && res.body.message ? res.body.message: res.body;
                  this.DatabaseCreationObj=this.DatabaseCreationForUser();
                  this.saveForm.DatabaseCreation(this.DatabaseCreationObj).subscribe((respons : any)=>{
                      ;
                      if(respons){
                        sessionStorage.setItem('customerId', respons.body.customerId);
                        this.ImageShown=2;
                        this.successMessage='Database Creation Done.';
                          this.TemplateCreationOBJ=this.ObjectCreationForTemplate();
                          this.saveForm.TemplateCreation(this.TemplateCreationOBJ).subscribe((result:any)=>{
                              if(result && result.status==200){
                                this.ImageShown=3;
                                this.TemplateLink=result.body.URL;
                                sessionStorage.setItem('TemplateLink', this.TemplateLink);
                                this.successMessage='Template Creation done.'
                                  this.PreFormApiCallOBJ=this.ObjectCreationforPreFormApiCall();
                                  setTimeout(() => {
                                    this.saveForm.PreFormGetOccupiedCIDRBlock(this.PreFormApiCallOBJ).subscribe((response:any)=>{
                                      if(response && response.status==200){
                                        this.ImageShown=4;
                                        this.successMessage=response.body.message;
                                        //this.TemplateLink=response;
                                        this.loading=false;
                                          this.next();
                                      }
                                  },(error: any) => {
                                    this.loading=false;
                                      var obj={
                                          onPage : 2,
                                          errormessage: error.error.message
                                      }
                                      this.ErrorOccursRedirectToDatabasePage.emit(obj);
                                      console.log("Error in PreForm", error);
                                  })
                                  }, 30000);
                              }
  
                          },(error: any) => {
                            this.loading=false;
                              var obj={
                                  onPage : 1,
                                  errormessage: error.error
                              }
                              this.ErrorOccursRedirectToDatabasePage.emit(obj);
                              console.log("Error in Template Creation", error);
                          })
                      }
                  },
                  (error: any) => {
                      ;
                      this.loading=false;
                      var obj={
                          onPage : 2,
                          errormessage: error.error.message
                      }
                      this.ErrorOccursRedirectToDatabasePage.emit(obj);
                      console.log("Error in Database Creation", error);
                  })
              }
          },(error: any) => {
              ;
              this.loading=false;
              var obj={
                  onPage : 2,
                  errormessage: error.error.message
              }
              this.ErrorOccursRedirectToDatabasePage.emit(obj);
              console.log("Error in VPC Peering", error);
          });
        }else{
                this.DatabaseCreationObj=this.DatabaseCreationForUser();
                this.saveForm.DatabaseCreation(this.DatabaseCreationObj).subscribe((respons : any)=>{
                    ;
                    if(respons && respons.status==200){
                      sessionStorage.setItem('customerId', respons.body.customerId);
                      this.ImageShown=2;
                      this.successMessage=respons.body.message;
                        this.TemplateCreationOBJ=this.ObjectCreationForTemplate();
                        this.saveForm.TemplateCreation(this.TemplateCreationOBJ).subscribe((result:any)=>{
                            if(result && result.status==200){
                              this.ImageShown=3;
                              this.TemplateLink=result.body.URL;
                              sessionStorage.setItem('TemplateLink', this.TemplateLink);
                              this.successMessage='Template Creation done.'
                                this.PreFormApiCallOBJ=this.ObjectCreationforPreFormApiCall();
                                this.saveForm.PreFormGetOccupiedCIDRBlock(this.PreFormApiCallOBJ).subscribe((response:any)=>{
                                    if(response && response.status==200){
                                      this.ImageShown=4;
                                      this.successMessage=response.body.message;
                                      //this.TemplateLink=response;
                                      this.loading=false;
                                        this.next();
                                    }
                                },(error: any) => {
                                  this.loading=false;
                                    var obj={
                                        onPage : 2,
                                        errormessage: error.error.message
                                    }
                                    this.ErrorOccursRedirectToDatabasePage.emit(obj);
                                    console.log("Error in PreForm", error);
                                })
                            }

                        },(error: any) => {
                          this.loading=false;
                            var obj={
                                onPage : 1,
                                errormessage: error.error
                            }
                            this.ErrorOccursRedirectToDatabasePage.emit(obj);
                            console.log("Error in Template Creation", error);
                        })
                    }
                },
                (error: any) => {
                    ;
                    this.loading=false;
                    var obj={
                        onPage : 2,
                        errormessage: error.error.message
                    }
                    this.ErrorOccursRedirectToDatabasePage.emit(obj);
                    console.log("Error in Database Creation", error);
                })
            }
    }



    ObjectCreationForVPCPeering(){
      let obj={} as any;
      obj.role=this.cookieService.get('CrossAccountRoleARN');
      obj.region=this.cookieService.get('Region');
      obj.vpcId=this.cookieService.get('VPCId');
      obj.routetableId=this.cookieService.get('RouteTableID');
      obj.cidrIP=this.cookieService.get('CIDRIP');
      obj.sgId=this.cookieService.get('SecurityGroupId');      
      return obj;

  }

  DatabaseCreationForUser(){
    let obj={} as any;
    obj.rdsPort= this.cookieService.get('Port');
    obj.rdsHost=this.cookieService.get('HostName');
    obj.rdsUser=this.cookieService.get('UserName');
    obj.rdsPassword=this.cookieService.get('Password');
    obj.companyName=this.cookieService.get('CompanyName');
    return obj;
}

  ObjectCreationForTemplate(){
    let obj={} as any;
    obj.awsAccountNo=this.cookieService.get('AWSAccountNumber');
    obj.companyName=this.cookieService.get('CompanyName');
    obj.instanceARN=this.cookieService.get('InstanceARN');
    obj.ctrStream=this.cookieService.get('CtrKinesisARN');
    obj.eventStream=this.cookieService.get('AgentKinesisARN');
    obj.region=this.cookieService.get('Region');
    obj.rdsPort=this.cookieService.get('Port');
    obj.rdsHost=this.cookieService.get('HostName');
    obj.rdsUser=this.cookieService.get('UserName');
    obj.rdsPassword=this.cookieService.get('Password');
    obj.sgId=this.cookieService.get('SecurityGroupIdForLambda');
    obj.subnetId=this.cookieService.get('Subnet');
    return obj;
  }

  ObjectCreationforPreFormApiCall(){
      let obj={} as any;
      obj.omniCustomerID=sessionStorage.getItem('customerId');
      obj.awsCustomerID=sessionStorage.getItem('customerId');
      obj.companyName=this.cookieService.get('CompanyName');
      obj.deploymentStage=this.cookieService.get('DeploymentStage');
      obj.subDomain=this.cookieService.get('SubDomain');
      obj.URL='https://' + this.cookieService.get('SubDomain') + '.omningageaws.com'
      obj.awsAccountNo=this.cookieService.get('AWSAccountNumber');
      obj.ccpURL=this.cookieService.get('ccpURL');
      obj.name='';
      obj.email=this.cookieService.get('CompanyEmail');
      obj.contact=this.cookieService.get('CountryCode') + this.cookieService.get('Contact');
      obj.country=this.cookieService.get('Country');
      obj.city=this.cookieService.get('City');
      obj.state=this.cookieService.get('State');
      obj.postCode=this.cookieService.get('ZIPCode');
      obj.businessInfo=this.cookieService.get('BusinessAddress');
      obj.instanceName=this.cookieService.get('InstanceName');
      obj.crossAccountRole=this.cookieService.get('CrossAccountRoleARN');
      obj.region=this.cookieService.get('Region');
      obj.instanceARN=this.cookieService.get('InstanceARN');
      obj.ctrStream=this.cookieService.get('CtrKinesisARN');
      obj.eventStream=this.cookieService.get('AgentKinesisARN');
      if(this.cookieService.get('S3Type') == 'Yes'){
      obj.callRecordingsBucketARN=this.cookieService.get('S3BucketCallRecording');
      obj.chatTranscriptsBucketARN=this.cookieService.get('S3BucketCallRecording');
      obj.attachmentsBucketARN=this.cookieService.get('S3BucketCallRecording');
    }
    else{
        obj.callRecordingsBucketARN=this.cookieService.get('S3BucketCallRecording');
        obj.chatTranscriptsBucketARN=this.cookieService.get('S3BucketChatAttachment');
        obj.attachmentsBucketARN=this.cookieService.get('S3BucketChatTranscript');   
    }
      obj.rdsHostName=this.cookieService.get('HostName');
      obj.rdsPort=this.cookieService.get('Port');
      obj.rdsUserName=this.cookieService.get('UserName');
      obj.rdsPassword=this.cookieService.get('Password');
      obj.vpcId=this.cookieService.get('VPCId');
      obj.cidrIP=this.cookieService.get('CIDRIP');
      obj.securityGroupIdLambda=this.cookieService.get('SecurityGroupIdForLambda');
      obj.subnetIdLambda=this.cookieService.get('Subnet');
      obj.securityGroupId=this.cookieService.get('SecurityGroupId');
      obj.routeTableId=this.cookieService.get('RouteTableID');

      return obj;

  }


  CheckIsPrivateEmail(EmailString){
    ;
    if(EmailString){
      for(var i=0;i<this.PersonalEmailList.length;i++){
        var result=EmailString.substring(EmailString.indexOf('@') + 1);
        if(result==this.PersonalEmailList[i]){
          return false;
        }
      }
      return true;
    }
  }


  selectedCountryPhoneNumber: string;
  cities: any[] = [];
  states: any[] = [];


  SelectedCountry() {
    this.omniCustomerDetails.State = null;
    this.omniCustomerDetails.City = null;
    this.states = [];
    this.cities = [];
    let selectedCountry: any = this.countries.find(
      (item) => item.name === this.omniCustomerDetails.Country
    );
    if (selectedCountry) {
      this.omniCustomerDetails.Contact='';
      this.states = csc.getStatesOfCountry(selectedCountry.id);
      this.selectedCountryPhoneNumber = '+' + selectedCountry.phonecode;
      this.CountryPhoneCode=this.selectedCountryPhoneNumber;
      //this.omniCustomerDetails.Contact = this.selectedCountryPhoneNumber;
    }
  }
  SelectedState() {
    this.omniCustomerDetails.City = null;
    this.cities = [];
    let selectedState: any = this.states.find(
      (item) => item.name === this.omniCustomerDetails.State
    );
    if (selectedState) {
      this.cities = csc.getCitiesOfState(selectedState.id);
    }
  }

}
