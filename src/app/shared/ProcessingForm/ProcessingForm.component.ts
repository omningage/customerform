import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ProcessingForm',
  templateUrl: './ProcessingForm.component.html',
  styleUrls: ['./ProcessingForm.component.scss']
})
export class ProcessingFormComponent implements OnInit {
  @Input() color?: string = '#000000';
  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(){
  }

}
