import { Component, EventEmitter, HostListener, OnInit, Output } from "@angular/core";
import csc from 'country-state-city';
import { CookieService } from "ngx-cookie-service";
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { ConfigService } from "src/services/config.service";
import { FormService } from 'src/services/form.service';
import { SaveFormService } from "src/services/SaveForm.service";

@Component({
    selector: "app-BussinessInformationDetails",
    templateUrl: "./BussinessInformationDetails.component.html",
    styleUrls: ["./BussinessInformationDetails.component.scss"],
})
export class BussinessInformationDetailsComponent implements OnInit {
  currentYear = new Date().getFullYear();
  apiError: string;

  @Output() nextButtonEventBussinessInformationType = new EventEmitter<number>();
  @Output() prevButtonEventBussinessInformationType = new EventEmitter<number>();


  nextButtonFade:boolean=false;
  message: number=0;
  countries: any[] = [];
  selectedCountryPhoneNumber: string;
  cities: any[] = [];
  states: any[] = [];
  enviormentDropdown: boolean = false;
  omniCustomerDetails: OmniCustomerDetail = new OmniCustomerDetail();
  errors: OmniCustomerDetail = new OmniCustomerDetail();
  formSubmitted: boolean = false;

  CompanyObject={} as any;

  PersonalEmailList=['gmail.com', 'outlook.com','hotmail.com', 'yahoo.com', 'proton.com'];
  subscriptions = [];

  @HostListener('document:click', ['$event'])
  onDocumentClick(event) {
    this.enviormentDropdown = false;
  }

  // @HostListener('keydown', ['$event']) onKeyDown(event) {
  //   ;
  //   let e = <KeyboardEvent> event;
  //   if (this.omniCustomerDetails.Contact.indexOf(parse(e.key)) == 0){
  //     this.omniCustomerDetails.Contact='';
  //   } 
  // }
  constructor(public formService: FormService, private cookieService: CookieService, private saveForm: SaveFormService, public config:ConfigService) {}

  ngOnInit() {
    this.countries = csc.getAllCountries();
    console.log('countries', this.countries);
    if(this.cookieService.get('CompanyName')){
      this.omniCustomerDetails.CompanyName=this.cookieService.get('CompanyName');
      this.omniCustomerDetails.Email=this.cookieService.get('CompanyEmail');
      this.omniCustomerDetails.Country=this.cookieService.get('Country');
      this.omniCustomerDetails.State=this.cookieService.get('State');
      this.omniCustomerDetails.City=this.cookieService.get('City');
      this.omniCustomerDetails.Contact=this.cookieService.get('Contact');
      this.CountryPhoneCode=this.cookieService.get('CountryCode');
      this.omniCustomerDetails.ZIP_PostCode=this.cookieService.get('ZIPCode');
      this.omniCustomerDetails.BusinessAddress=this.cookieService.get('BusinessAddress');
      this.nextButtonFade=true;
    }
    this.subscriptions.push(
      this.config.onConfigRead.subscribe(() => {
        this.CallCompanyName();
      })
    );
  }

  next() {
    this.nextButtonEventBussinessInformationType.emit(2)
  }

  prev() {
    this.prevButtonEventBussinessInformationType.emit(0)
  }

  CallCompanyName(){
    this.saveForm.getCompanyName().subscribe((result:any)=>{
        if(result){

            this.CompanyObject = result;
            console.log('this.CompanyObject',this.CompanyObject);
        }

    });
}


  submitBusinessInfo(forButtonClick:boolean): void {
    var RegularExpression=/^[1-9][0-9]{0,9}$/;
    this.errors = new OmniCustomerDetail();
    if (
      this.omniCustomerDetails.CompanyName == null ||
      this.omniCustomerDetails.CompanyName == ''
    ) {
      this.errors.CompanyName = this.formService.requiredFieldError;
      return;
    }else if(this.CompanyObject.find((item:any)=> item.CompanyName == this.omniCustomerDetails.CompanyName)){
      this.errors.CompanyName =
        'Already exists. Please try with another name.';
        return;
    }
     else if (this.omniCustomerDetails.CompanyName.length > 100) {
      this.errors.CompanyName =
        'Should be less than or equal to 100 characters.';
      return;
    } else if (
      this.omniCustomerDetails.Email == null ||
      this.omniCustomerDetails.Email == ''
    ) {
      this.errors.Email = this.formService.requiredFieldError;
      return;
    } else if (
      !this.formService.emailRegex.test(this.omniCustomerDetails.Email)
    ) {
      this.errors.Email = 'Enter a valid email.';
      return;
    }else if(!this.CheckIsPrivateEmail(this.omniCustomerDetails.Email)){
      this.errors.Email="Please enter company email."
      return;
    }
     else if (
      this.omniCustomerDetails.Country == null ||
      this.omniCustomerDetails.Country == ''
    ) {
      this.errors.Country = this.formService.requiredFieldError;
      return;
    } else if (
      !this.countries.find(
        (item) => item.name === this.omniCustomerDetails.Country
      )
    ) {
      this.errors.Country = 'Select a given Country.';
      return;
    } else if (
      this.omniCustomerDetails.State == null ||
      this.omniCustomerDetails.State == ''
    ) {
      this.errors.State = this.formService.requiredFieldError;
      return;
    }else if (
      this.omniCustomerDetails.City == null ||
      this.omniCustomerDetails.City == ''
    ) {
      this.errors.City = this.formService.requiredFieldError;
      return;
    } else if (
      this.omniCustomerDetails.Contact == null ||
      this.omniCustomerDetails.Contact == ''
    ) {
      this.errors.Contact = this.formService.requiredFieldError;
      return;
    } else if (
      /^\d{7,}$/.test(
        this.omniCustomerDetails.Contact.replace(/[\s()+\-\.]|ext/gi, '')
      ) == false
    ) {
      this.errors.Contact = 'Not a valid contact.';
      return;
    } else if(!RegularExpression.test(this.omniCustomerDetails.Contact)){
      this.errors.Contact = 'Not a valid contact.';
      return;
    }
    else if (
      this.omniCustomerDetails.ZIP_PostCode == null ||
      this.omniCustomerDetails.ZIP_PostCode == ''
    ) {
      this.errors.ZIP_PostCode = this.formService.requiredFieldError;
      return;
    } 
    //else if (
    //   !this.formService.numbersRegex.test(this.omniCustomerDetails.ZIP_PostCode)
    // ) {
    //   this.errors.ZIP_PostCode = 'Not a valid zip code.';
    //   return;
    // } 
    else  if (
      this.omniCustomerDetails.BusinessAddress == null ||
      this.omniCustomerDetails.BusinessAddress == ''
    ) {
      this.errors.BusinessAddress = this.formService.requiredFieldError;
      return;
    } else if (this.omniCustomerDetails.BusinessAddress.length > 250) {
      this.errors.CompanyName =
        'Should be less than or equal to 250 characters.';
      return;
    }
    console.log(this.omniCustomerDetails);
    if(forButtonClick==true){
      this.SetBusinessInformation();
    }
  }

  SetBusinessInformation(){
    this.cookieService.set('CompanyName', this.omniCustomerDetails.CompanyName);
    this.cookieService.set('CompanyEmail', this.omniCustomerDetails.Email);
    this.cookieService.set('Country', this.omniCustomerDetails.Country);
    this.cookieService.set('State', this.omniCustomerDetails.State);
    this.cookieService.set('City', this.omniCustomerDetails.City);
    this.cookieService.set('ZIPCode', this.omniCustomerDetails.ZIP_PostCode);
    this.cookieService.set('Contact', this.omniCustomerDetails.Contact);
    this.cookieService.set('CountryCode', this.CountryPhoneCode);
    this.cookieService.set('BusinessAddress', this.omniCustomerDetails.BusinessAddress);
    this.next();
  }

  public CountryPhoneCode='+1';

  SelectedCountry() {
    this.omniCustomerDetails.State = null;
    this.omniCustomerDetails.City = null;
    this.states = [];
    this.cities = [];
    let selectedCountry: any = this.countries.find(
      (item) => item.name === this.omniCustomerDetails.Country
    );
    if (selectedCountry) {
      this.omniCustomerDetails.Contact='';
      this.states = csc.getStatesOfCountry(selectedCountry.id);
      this.selectedCountryPhoneNumber = '+' + selectedCountry.phonecode;
      this.CountryPhoneCode=this.selectedCountryPhoneNumber;
      //this.omniCustomerDetails.Contact = this.selectedCountryPhoneNumber;
    }
  }
  SelectedState() {
    this.omniCustomerDetails.City = null;
    this.cities = [];
    let selectedState: any = this.states.find(
      (item) => item.name === this.omniCustomerDetails.State
    );
    if (selectedState) {
      this.cities = csc.getCitiesOfState(selectedState.id);
    }
  }

  CheckIsPrivateEmail(EmailString){
    if(EmailString){
      for(var i=0;i<this.PersonalEmailList.length;i++){
        var result=EmailString.substring(EmailString.indexOf('@') + 1);
        if(result==this.PersonalEmailList[i]){
          return false;
        }
      }
      return true;
    }
  }

  CheckContinueButton(){
    this.nextButtonFade=false;
    if(this.omniCustomerDetails.CompanyName && this.omniCustomerDetails.Email && this.omniCustomerDetails.Country && this.omniCustomerDetails.State && this.omniCustomerDetails.City && this.omniCustomerDetails.ZIP_PostCode && this.omniCustomerDetails.Contact && this.omniCustomerDetails.BusinessAddress){
      this.nextButtonFade=true;
    }
  }

}
