import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
    selector: "app-SubmissionForm",
    templateUrl: "./SubmissionForm.component.html",
    styleUrls: ["./SubmissionForm.component.scss"],
})
export class SubmissionFormComponent implements OnInit {
    @Input() public TemplateLinkSubmission: string='';
    constructor() {}


    ngOnInit(): void {

    }

    ngOnChanges(){
        this.TemplateLinkSubmission=this.TemplateLinkSubmission;
        console.log('TemplateLinkSubmission',this.TemplateLinkSubmission);
    }

    openLink(){
        window.open(this.TemplateLinkSubmission);
    }

    copyText(val: string){
        let selBox = document.createElement('textarea');
          selBox.style.position = 'fixed';
          selBox.style.left = '0';
          selBox.style.top = '0';
          selBox.style.opacity = '0';
          selBox.value = val;
          document.body.appendChild(selBox);
          selBox.focus();
          selBox.select();
          document.execCommand('copy');
          document.body.removeChild(selBox);
        }
}
