import { Component, EventEmitter, HostListener, OnInit, Output } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { FormService } from 'src/services/form.service';
import {SaveFormService} from 'src/services/SaveForm.service';

@Component({
    selector: "app-S3BucketInformationForm",
    templateUrl: "./S3BucketInformationForm.component.html",
    styleUrls: ["./S3BucketInformationForm.component.scss"],
})
export class S3BucketInformationFormComponent implements OnInit {
    currentYear = new Date().getFullYear();
    omniCustomerDetails: OmniCustomerDetail = new OmniCustomerDetail();
    errors: OmniCustomerDetail = new OmniCustomerDetail();
    @Output() nextButtonEventS3InformationType = new EventEmitter<number>();
    @Output() prevButtonEventS3InformationType = new EventEmitter<number>();
    regions: any[] = [];
    nextButtonFade:boolean=false;

   TemplateLink='';

   loading=false;

    S3Bucket:any[]=[
        {S3: 'Yes'},
        {S3: 'No'}
    ];

    S3Dropdown:boolean=false;
    constructor(private saveForm: SaveFormService, public formService: FormService, private cookieService: CookieService) {}
    ngOnInit(): void {

        if(this.cookieService.get('AWSAccountNumber')){
            this.omniCustomerDetails.S3Type=this.cookieService.get('S3Type');
            this.omniCustomerDetails.BucketNameForCallRecording=this.cookieService.get('S3BucketCallRecording');
            if(this.omniCustomerDetails.S3Type=='No'){
                this.omniCustomerDetails.BucketNameForChatAttachment=this.cookieService.get('S3BucketChatAttachment');
                this.omniCustomerDetails.BucketNameForChatTranscript=this.cookieService.get('S3BucketChatTranscript');
            }
            this.nextButtonFade=true;
            
        }
    }


    @HostListener('document:click', ['$event'])
    onDocumentClick(event) {
    this.S3Dropdown=false;
  }

  


  SelectedS3Type(index) {
    this.omniCustomerDetails.S3Type = this.S3Bucket[index].S3;
    this.omniCustomerDetails.BucketNameForChatAttachment='';
    this.omniCustomerDetails.BucketNameForCallRecording='';
    this.omniCustomerDetails.BucketNameForChatTranscript='';
    this.S3Dropdown = false;
    this.nextButtonFade=false;
  }
  submitS3BucketDetails() {
        this.errors = new OmniCustomerDetail();
        if(this.omniCustomerDetails.S3Type == null || this.omniCustomerDetails.S3Type == ''){
          this.errors.S3Type= this.formService.requiredFieldError;
          return;
        }
        if(this.omniCustomerDetails.S3Type== 'Yes'){
          if (
            !this.omniCustomerDetails.BucketNameForCallRecording ||
            this.omniCustomerDetails.BucketNameForCallRecording.length === 0
            ) {
              this.errors.BucketNameForCallRecording = this.formService.requiredFieldError;
              return;
            }
          }
          else if(this.omniCustomerDetails.S3Type== 'No'){
            if (
              !this.omniCustomerDetails.BucketNameForCallRecording ||
              this.omniCustomerDetails.BucketNameForCallRecording.length === 0
              ) {
                this.errors.BucketNameForCallRecording = this.formService.requiredFieldError;
                return;
              }
              else  if (
                !this.omniCustomerDetails.BucketNameForChatTranscript ||
                this.omniCustomerDetails.BucketNameForChatTranscript.length === 0
              ) {
                this.errors.BucketNameForChatTranscript = this.formService.requiredFieldError;
                return;
              }
              else  if (
                !this.omniCustomerDetails.BucketNameForChatAttachment ||
                this.omniCustomerDetails.BucketNameForChatAttachment.length === 0
              ) {
                this.errors.BucketNameForChatAttachment = this.formService.requiredFieldError;
                return;
              }

          }
        console.log(this.omniCustomerDetails);
        this.SetAWSConnectForm();
      }

     
      SetAWSConnectForm(){
          this.cookieService.set('S3Type',this.omniCustomerDetails.S3Type);
          this.cookieService.set('S3BucketCallRecording', this.omniCustomerDetails.BucketNameForCallRecording);
          if(this.omniCustomerDetails.S3Type=='No'){
            this.cookieService.set('S3BucketChatAttachment', this.omniCustomerDetails.BucketNameForChatAttachment);
            this.cookieService.set('S3BucketChatTranscript', this.omniCustomerDetails.BucketNameForChatTranscript);
          }
          this.next()
    }
    
    next() {
        this.nextButtonEventS3InformationType.emit(4)
      }
      prev() {
        this.prevButtonEventS3InformationType.emit(2)
      }


      CheckContinueButton(){
        this.nextButtonFade=false;
        if(this.omniCustomerDetails.S3Type){
          if(this.omniCustomerDetails.S3Type=='Yes'){
            if(this.omniCustomerDetails.BucketNameForCallRecording){
              this.nextButtonFade=true;
            }
          }else if(this.omniCustomerDetails.S3Type=='No'){
            if(this.omniCustomerDetails.BucketNameForCallRecording && this.omniCustomerDetails.BucketNameForChatAttachment && this.omniCustomerDetails.BucketNameForChatTranscript){
              this.nextButtonFade=true;
            }
          }
        }
          
        
      }


}
