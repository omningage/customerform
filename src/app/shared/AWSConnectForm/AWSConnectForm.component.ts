import { Component, EventEmitter, HostListener, OnInit, Output } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { ConfigService } from "src/services/config.service";
import { FormService } from 'src/services/form.service';
import {SaveFormService} from 'src/services/SaveForm.service';

@Component({
    selector: "app-AWSConnectForm",
    templateUrl: "./AWSConnectForm.component.html",
    styleUrls: ["./AWSConnectForm.component.scss"],
})
export class AWSConnectFormComponent implements OnInit {
    currentYear = new Date().getFullYear();
    omniCustomerDetails: OmniCustomerDetail = new OmniCustomerDetail();
    errors: OmniCustomerDetail = new OmniCustomerDetail();
    @Output() nextButtonEventAmazonConnectType = new EventEmitter<number>();
    @Output() prevButtonEventAmazonConnectType = new EventEmitter<number>();
    regions: any[] = [];
    nextButtonFade:boolean=false;

   TemplateLink='';

   loading=false;
   successMessage="VPC Peering Intialized.";

    S3Bucket:any[]=[
        {S3: 'Yes'},
        {S3: 'No'}
    ];
    subscriptions=[] as any;
    regionCode:string;
    RegionDropdown: boolean = false;
    S3Dropdown:boolean=false;
    constructor(private saveForm: SaveFormService, public formService: FormService, private cookieService: CookieService, public config:ConfigService) {}
    ngOnInit(): void {
      if(this.cookieService.get('AWSAccountNumber')){
        this.omniCustomerDetails.AWSAccountNumber=this.cookieService.get('AWSAccountNumber');
        this.omniCustomerDetails.ccpURL=this.cookieService.get('ccpURL');
        this.omniCustomerDetails.InstanceName=this.cookieService.get('InstanceName');
        this.omniCustomerDetails.CrossAccountRoleARN=this.cookieService.get('CrossAccountRoleARN');
        this.omniCustomerDetails.Region=this.cookieService.get('RegionName');
        this.omniCustomerDetails.InstanceARN=this.cookieService.get('InstanceARN');
        this.omniCustomerDetails.CtrKinesisARN=this.cookieService.get('CtrKinesisARN');
        this.omniCustomerDetails.AgentKinesisARN=this.cookieService.get('AgentKinesisARN');
        this.regionCode=this.cookieService.get('Region');
        this.nextButtonFade=true;
        
    }

    this.subscriptions.push(
      this.config.onConfigRead.subscribe(() => {
        this.gettingRegions()
      })
    );
  }


    @HostListener('document:click', ['$event'])
    onDocumentClick(event) {
    this.RegionDropdown = false;
  }

  gettingRegions(){
      this.regions=JSON.parse(sessionStorage.getItem("RegionsArray"));
      this.saveForm.getRegions().subscribe( (result:any) =>{
        if(result){
          this.regions=result;
          sessionStorage.setItem("RegionsArray", JSON.stringify(this.regions));
      }
      console.log(result);
    });
  }

  SelectedRegionType(index) {
    this.omniCustomerDetails.Region = this.regions[index].region;
    this.regionCode=this.regions[index].code;
    this.RegionDropdown = false;
  }

    submitConnectAccountDetail() {
        this.errors = new OmniCustomerDetail();
        if(this.omniCustomerDetails.CrossAccountRoleARN){
          var resultCrossAccountValidation=this.omniCustomerDetails.CrossAccountRoleARN.match('arn:aws:iam::' + this.omniCustomerDetails.AWSAccountNumber + ':role/');
        }
        if(this.omniCustomerDetails.ccpURL){
          var resultCCPURLV1=this.omniCustomerDetails.ccpURL.match('.awsapps.com');
          if(!resultCCPURLV1){
            resultCCPURLV1=this.omniCustomerDetails.ccpURL.match('.my.connect.aws/');
          }
        }
        if(this.omniCustomerDetails.InstanceARN){
          var resultInstanceARN=this.omniCustomerDetails.InstanceARN.match('arn:aws:connect:'+ this.regionCode + ':' + this.omniCustomerDetails.AWSAccountNumber + ':instance/');
        }
        if (
            !this.omniCustomerDetails.AWSAccountNumber ||
            this.omniCustomerDetails.AWSAccountNumber.length === 0
          ) {
            this.errors.AWSAccountNumber = this.formService.requiredFieldError;
            return;
          } else if (!this.formService.numbersRegex.test(this.omniCustomerDetails.AWSAccountNumber)) {
            this.errors.AWSAccountNumber = this.formService.requiredFieldError;
            return;
          }else if (this.omniCustomerDetails.AWSAccountNumber.length!=12){
              this.errors.AWSAccountNumber="Please add 12 digits account number."
              return;
          }else if (
            !this.omniCustomerDetails.ccpURL ||
            this.omniCustomerDetails.ccpURL.length === 0
          ) {
            this.errors.ccpURL = this.formService.requiredFieldError;
            return;
          } else if (
            !this.formService.urlRegex.test(this.omniCustomerDetails.ccpURL)
          ) {
            this.errors.ccpURL = 'Enter a valid URL';
            return;
          }else if(!resultCCPURLV1){
            this.errors.ccpURL='Please enter a valid URL.';
            return;
          }
          else if (
          !this.omniCustomerDetails.InstanceName ||
          this.omniCustomerDetails.InstanceName.length === 0
        ) {
          this.errors.InstanceName = this.formService.requiredFieldError;
          return;
        } else if(!this.omniCustomerDetails.CrossAccountRoleARN || this.omniCustomerDetails.CrossAccountRoleARN.length==0){
            this.errors.CrossAccountRoleARN=this.formService.requiredFieldError;
            return;
        } else if(!resultCrossAccountValidation){
          this.errors.CrossAccountRoleARN='Please enter a valid Cross Account Role ARN.';
          return;
        }
        else if (
            this.omniCustomerDetails.Region == null ||
            this.omniCustomerDetails.Region == ''
          ) {
            this.errors.Region = 'Region is required.';
            return;
          } else if (
          !this.omniCustomerDetails.InstanceARN ||
          this.omniCustomerDetails.InstanceARN.length === 0
        ) {
          this.errors.InstanceARN = this.formService.requiredFieldError;
          return;
        }else if(!resultInstanceARN){
          this.errors.InstanceARN='Please enter a valid Instance ARN.';
          return;
        } 
        else if (
          !this.omniCustomerDetails.CtrKinesisARN ||
          this.omniCustomerDetails.CtrKinesisARN.length === 0
        ) {
          this.errors.CtrKinesisARN = this.formService.requiredFieldError;
          return;
        }
        else if (
            !this.omniCustomerDetails.AgentKinesisARN ||
            this.omniCustomerDetails.AgentKinesisARN.length === 0
          ) {
            this.errors.AgentKinesisARN = this.formService.requiredFieldError;
            return;
          }
        console.log(this.omniCustomerDetails);
        this.SetAWSConnectForm();
      }

     
      SetAWSConnectForm(){
        this.cookieService.set('AWSAccountNumber', this.omniCustomerDetails.AWSAccountNumber);
        this.cookieService.set('ccpURL', this.omniCustomerDetails.ccpURL);
        this.cookieService.set('InstanceName', this.omniCustomerDetails.InstanceName);
        this.cookieService.set('CrossAccountRoleARN', this.omniCustomerDetails.CrossAccountRoleARN);
        this.cookieService.set('RegionName', this.omniCustomerDetails.Region);
        this.cookieService.set('Region', this.regionCode);
        this.cookieService.set('InstanceARN', this.omniCustomerDetails.InstanceARN);
        this.cookieService.set('CtrKinesisARN', this.omniCustomerDetails.CtrKinesisARN);
        this.cookieService.set('AgentKinesisARN', this.omniCustomerDetails.AgentKinesisARN);

          this.next();
         


        //this.loading=false;
    }

      next() {
        
        this.nextButtonEventAmazonConnectType.emit(3)
      }
    
      prev() {
        this.prevButtonEventAmazonConnectType.emit(1)
      }

      CheckContinueButton(){
        this.nextButtonFade=false;
        if(this.omniCustomerDetails.AWSAccountNumber && this.omniCustomerDetails.ccpURL && this.omniCustomerDetails.InstanceName && this.omniCustomerDetails.CrossAccountRoleARN && this.omniCustomerDetails.Region && this.omniCustomerDetails.InstanceARN && this.omniCustomerDetails.CtrKinesisARN && this.omniCustomerDetails.AgentKinesisARN){
          this.nextButtonFade=true;
        }
      }


}
