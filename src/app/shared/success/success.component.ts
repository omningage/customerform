import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ConfigService } from 'src/services/config.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
  foradmin:string='';
  forsupervisor:string='';
  subscriptions=[] as any;
  @Output() prevButtonEventSuccessForm = new EventEmitter<number>();

  constructor(private cookieService: CookieService, private config: ConfigService) { }

  ngOnInit(): void {
    this.subscriptions.push(
      this.config.onConfigRead.subscribe((res:any) => {
          this.forsupervisor = 'https://' + this.cookieService.get('SubDomain') + '.omningageaws.com'
          this.foradmin=res.admin_url;
      })
    );
  }

  prev() {
    this.prevButtonEventSuccessForm.emit(6)
  }

}
