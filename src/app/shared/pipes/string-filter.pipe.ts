import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringFilter'
})
export class StringFilterPipe implements PipeTransform {
  transform(array: any[], key: string, searchText: string): any[] {
    console.log("PIPE", key, searchText)
    if (!array) return [];
    if (!searchText) return array;
    searchText = searchText.toLowerCase();
    return array.filter((item) => {
      return item[key].toLowerCase().includes(searchText);
    });
  }
}
