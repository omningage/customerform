import { Component, EventEmitter, HostListener, OnInit, Output } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { ConfigService } from "src/services/config.service";
import { FormService } from 'src/services/form.service';
import { SaveFormService } from "src/services/SaveForm.service";
//import { CookieService } from "ngx-cookie-service";

@Component({
    selector: "app-BussinessInformation",
    templateUrl: "./BussinessInformation.component.html",
    styleUrls: ["./BussinessInformation.component.scss"],
})
export class BussinessInformationComponent implements OnInit {
  currentYear = new Date().getFullYear();
    constructor(public formService: FormService,private cookieService: CookieService, public config:ConfigService, private saveForm: SaveFormService,) {}
    @Output() nextButtonEventBussinessType = new EventEmitter<number>();
    forNextButton:number = 1;
    omniCustomerDetails: OmniCustomerDetail = new OmniCustomerDetail();
    errors: OmniCustomerDetail = new OmniCustomerDetail();
    enviormentDropdown: boolean = false;

    SubDomainList=[] as any;

    nextButtonFade:boolean=false;
    subscriptions = [];

    Environment: any[] = [
      { environment: 'Trial (14 days free trial)' },
      { environment: 'Business (This is paid premium account)' },
    ];

    @HostListener('document:click', ['$event'])
    onDocumentClick(event) {
    this.enviormentDropdown = false;
  }


    ngOnInit(): void {

      this.subscriptions.push(
        this.config.onConfigRead.subscribe(() => {
          this.CallSubDomain();
        })
      );
      if(this.cookieService.get('DeploymentStage')){
        this.omniCustomerDetails.DeploymentStage=this.cookieService.get('DeploymentStage')//this.cookieService.get('DeploymentStage');
        this.omniCustomerDetails.SubDomain=this.cookieService.get('SubDomain');
        this.nextButtonFade=true;
      }
    }

    CallSubDomain(){
      this.saveForm.getSubDomain().subscribe((result:any)=>{
          if(result){
              console.log('SUBDOAMIN--------------',result);
              this.SubDomainList=result;
          }
  
      });
  }

    SelectedEnvironment(index) {
      this.omniCustomerDetails.DeploymentStage = this.Environment[
        index
      ].environment;
      this.enviormentDropdown = false;
    }
    next():void {
      this.nextButtonEventBussinessType.emit(this.forNextButton);
    }

    submitBusinessInfo(): void {
      this.errors = new OmniCustomerDetail();

      if (
        !this.omniCustomerDetails.DeploymentStage ||
        this.omniCustomerDetails.DeploymentStage.length === 0
      ) {
        this.errors.DeploymentStage = this.formService.requiredFieldError;
        return;
      }
      else if (
        !this.omniCustomerDetails.SubDomain ||
        this.omniCustomerDetails.SubDomain.length === 0
      ) {
        this.errors.SubDomain = this.formService.requiredFieldError;
        return;
      }
      else if (
        this.omniCustomerDetails.SubDomain.length > 20
      ) {
        this.errors.SubDomain = "Sub domain should be less than 20 characters.";
        return;
      }
      else if(/[^a-zA-Z0-9]/.test(this.omniCustomerDetails.SubDomain)){
        this.errors.SubDomain = "Sub domain is not valid";
        return;
      }
      else if(this.SubDomainList.find((item:any)=> item.subDomain == this.omniCustomerDetails.SubDomain)){
        this.errors.SubDomain="This Domain already exists. Please try another sub domain"
        return;
      }



      this.SetDataDeploymentStage();
    }

    SetDataDeploymentStage(){
      this.cookieService.set('DeploymentStage', this.omniCustomerDetails.DeploymentStage);
      this.cookieService.set('SubDomain', this.omniCustomerDetails.SubDomain);
      this.next();
    }

    CheckContinueButton(){
      this.nextButtonFade=false;
      if(this.omniCustomerDetails.SubDomain){
        this.nextButtonFade=true;
      }
    }
}
