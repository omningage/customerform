export class OmniCustomerDetail {
  //Deployment Stage
  DeploymentStage: string;
  //business info
  CompanyName: string;
  Email: string;
  Contact: string;
  Country: string;
  City: string;
  State: string;
  ZIP_PostCode: string;
  BusinessAddress: string;
  // database info
  //DatabaseName: string;
  HostName: string;
  Port: string;
  UserName: string;
  Password: string;
  ISVPCOrPublic:string;
  VPCId:string;
  CIDRIP:string;
  SecurityGroupId:string;
  SecurityGroupIdForLambda:string;
  RouteTableID:string;
  SubnetID:string;
  //aws connect info
  AWSAccountNumber:string;
  ccpURL: string;
  InstanceName: string;
  CrossAccountRoleARN: string;
  Region: string;
  InstanceARN: string;
  CtrKinesisARN: string;
  AgentKinesisARN: string;
  S3Type:string;
  BucketNameForCallRecording:string;
  BucketNameForChatTranscript:string;
  BucketNameForChatAttachment:string;
  SubDomain:string;
  










  // //Deployment Stage
  // Region: string;
  // //business info
  // CompanyName: string;
  // DeploymentStage: string;
  // AWSAccountNo: string;
  // callRecordingsBucketARN: string;
  // chatTranscriptsBucketARN: string;
  // attachmentsBucketARN: string;
  // //aws connect info
  // InstanceName: string;
  // InstanceARN: string;
  // CrossAccountRoleARN: string;
  // CtrKinesisARN: string;
  // AgentKinesisARN: string;
  // ccpURL: string;
  // //personal info
  // Name: string;
  // Email: string;
  // Contact: string;
  // Country: string;
  // City: string;
  // State: string;
  // ZIP_PostCode: string;
  // BusinessInfo: string;
  // // database info
  // DatabaseName: string;
  // HostName: string;
  // Port: string;
  // UserName: string;
  // Password: string;
}
