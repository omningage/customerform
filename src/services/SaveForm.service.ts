import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Config } from "src/models/config.model";
import { ConfigService } from "src/services/config.service";

@Injectable({
    providedIn: "root",
})
export class SaveFormService {
    private baseUrl: string;
    subscriptions=[] as any;
    constructor(
        private httpClient: HttpClient,
        private config: ConfigService,
    ) {
        this.subscriptions.push(
            this.config.onConfigRead.subscribe((res:any) => {
                this.baseUrl = res.api_url;
            })
          );
    }


    //forGettingSubdomain
    getSubDomain(){
        return this.httpClient.get(this.baseUrl + "subdomain")
    }
   
    // For getting Regions
    getRegions() {
        return this.httpClient.get(this.baseUrl + "region");
    }

    // For getting CIDR
    getOccupiedCIDR() {
        ;
        return this.httpClient.get(this.baseUrl + "occupiedCIDR");
    }

    getCompanyName() {
        return this.httpClient.get(this.baseUrl + "getcompanyname");
    }

    VPCPeering(obj:any):  Observable<HttpResponse<Config>> {
        return this.httpClient.post<Config>(this.baseUrl + "vpcpeering", obj, {observe: 'response'});
    }

    DatabaseCreation(obj:any):  Observable<HttpResponse<Config>> {
        return this.httpClient.post<Config>(this.baseUrl + "database", obj, {observe: 'response'});
    }

    TemplateCreation(obj:any):  Observable<HttpResponse<Config>>{
        return this.httpClient.post<Config>(this.baseUrl + "template", obj, {observe: 'response'});
    }

    PreFormGetOccupiedCIDRBlock(obj:any):  Observable<HttpResponse<Config>>{ 
        return this.httpClient.post<Config>(this.baseUrl + 'preform', obj, {observe: 'response'})
    }

}
