import { Injectable, NgZone } from '@angular/core';
import { Subject , BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {Config} from 'src/models/config.model';

@Injectable({
	providedIn: 'root',
})
export class ConfigService {
	onConfigRead = new BehaviorSubject<any>([]);
    //onMasterConfigRead = new BehaviorSubject<any>([]);
	private _api_url: string = '';
	private _region: string = '';
	private _supervisor_url: string = '';
	private _admin_url: string = '';


	constructor(private http: HttpClient, private router:Router,private _ngZone:NgZone) {
		this.readConfig();
	}
	private readConfig() {
		;
		this.http.get('/assets/config/config.json').subscribe((config: Config) => {
			this._api_url = config.api_url;
			this._region = config.region;
			this._supervisor_url=config.supervisorUrl;
			this._admin_url=config.adminUrl;
			this.onConfigRead.next(config);
		});
	}

    



	get apiUrl(): string {
		return this._api_url;
	}
	get region(): string {
		return this._region;
	}

	get SupervisorUrl(): string {
		return this._supervisor_url;
	}
	get admin(): string {
		return this._admin_url;
	}
	
}
