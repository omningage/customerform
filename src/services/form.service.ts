import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OmniCustomerDetail } from 'src/models/omni-customer-detail.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  // public awsCustomerId: string = new Date().toISOString();
  // public awsProductCode: string = new Date().toISOString();
  tooltipPosition: 'before' | 'after' = 'after';
  //tooltip texts
  regionTooltip: string = 'Select your region from given list.';
  s3Bucket:string='Are you using one S3 bucket for call recording, chat transcripts, and attachments?';
  environmentTooltip: string = 'Environment for your instance.';
  accountNumberTooltip: string =
    'Amazon account number should be 12 digits number. e.g 123456789123';
  connectNumberTooltip: string =
    '36 digits alphnumeric number e.g 12345678-1234-1234-1234-123456789123';
  ccpUrlTooltip: string =
    'Url of contact control panel. e.g https://your-instance-name.awsapps.com/connect/ccp-v2';
  //business info tooltips
  instanceARNTooltip: string =
    'Complete instance ARN of your amazon connect account.';
  //amazon connect detail tooltips
  instanceNameTooltip: string = 'Your amazon connect complete instance name.';
  crossRoleTooltip: string =
    'Cross origin read-only role permission for our instance to sync your heirarchy.';
  ctrKenisisTooltip: string =
    'CTR Kenisis Stream ARN of your connect instance.';
  agentKenisisTooltip: string =
    'Agent Kenisis Stream ARN of your connect instance.';
    AwsAccountNumber: string =
    'A 12-digit number, such as 123456789012, that uniquely identifies an AWS account. Many AWS resources includes that account id in ther Amazon Resource Name (ARNs). The account id portion distinguishes resources in one account from the resources in another account.';
    awsconnectnumberTooltip: string =
    'AWS Connect Number.';
    companyNameTooltip: string =
    'Company Name';
    hostnameTooltip: string =
    'Host Name';
  s3BucketTooltip: string =
    'S3 Bucket(s) ARN for storing your data. separate them with ";" in case of multiple buckets.';
  userNameTooltip: string = 'Your UserName for database instance.';
  portTooltip: string = 'Port on which the database instance is running';
  callRecordingsBucketTooltip: string =
    'S3 Bucket ARN where you are storing your call recordings.';
  chatTranscriptsBucketTooltip: string =
    'S3 Bucket ARN where you are storing your chat transcripts.';
  attachmentsBucketTooltip: string =
    'S3 Bucket ARN where you are storing your attachments.';

  requiredFieldError: string = 'This field is required.';
  emailRegex: RegExp = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/;
  numbersRegex: RegExp = /^[0-9]+$/;
  urlRegex: RegExp = /(https:\/\/[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/[a-zA-Z0-9]+\.[^\s]{2,}|[a-zA-Z0-9]+\.[^\s]{2,})/;

  constructor(private http: HttpClient) {}

  subscribeOmniUser(body: OmniCustomerDetail) {
    return this.http.put(
      environment.baseUrl + environment.subscribeUserUrl,
      body
    );
  }
}
